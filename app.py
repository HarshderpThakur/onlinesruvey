#----------------------------------------------------------------------------#
# Imports
#----------------------------------------------------------------------------#

from flask import Flask, render_template, request, jsonify
import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode
import os
from flask_cors import CORS

# App Config.

app = Flask(__name__)
app.config.from_object('config')
CORS(app)
connection = mysql.connector.connect(host='localhost', database='pytonDb', user='root', password='password')


topic = "this is poll !!" 
multiselect = 1 

# Controllers.

@app.route('/home')
def home():
    return render_template('layouts/HTML/HomePage.html')
#   return render_template('layouts/HTML/home.html')

@app.route('/vote',methods = ['POST'])
def vote1():
    content = request.get_json()
    uid = content['id']
    sel_arr = content['vote']
    selections = ""
    for selection in sel_arr:
        selections += ",('" + uid + "','" + selection + "')" 
    selections = selections[1:]
    insertQuery = "INSERT INTO polls(uid, options) VALUE " + selections 
    print(insertQuery); 
    try:
        cursor = connection.cursor()
        for result in cursor.execute(insertQuery, multi=True):
            if result.with_rows:
                print("Rows produced by statement '{}':".format(
                result.statement))
                print(result.fetchall())
            else:
                print("Number of rows affected by statement '{}': {}".format(
                result.statement, result.rowcount))
        connection.commit()
        cursor.close()
        connection.close()
    except mysql.connector.Error as error :
        connection.rollback() #rollback if any exception occured
        print("Failed inserting record into python_users table {}".format(error))
    finally:
        #closing database connection.
        if(connection.is_connected()):
            cursor.close()
            connection.close()
            print("MySQL connection is closed")

    data = {'name': 'Alice', 'type': "userVOTE", 'content': insertQuery}
    return jsonify(data)

@app.route('/poll',methods = ['POST'])
def poll():
    content = request.get_json()['options']
    topic = request.get_json()['qus']
    multiselect = request.get_json()['multiselect']
    titleOPTS = "('"+topic+"','"+multiselect+"');"
    options = "" 
    for option in content:
        options += ",('"+option+"')"
    options = options[1:]
    print(options)

    try:
        pollQuery = """DROP TABLE IF EXISTS polls;
                       CREATE TABLE `polls` (
                           `id` int AUTO_INCREMENT,
                           `uid` varchar(254) NOT NULL,
                           `options` varchar(256) NOT NULL,
                                PRIMARY KEY (`id`)
                          );
                        DROP TABLE IF EXISTS options;
                        CREATE TABLE `options` (
                            `id` int AUTO_INCREMENT,
                            `name` varchar(256) NOT NULL,
                            PRIMARY KEY (`id`)
                          );
                        INSERT INTO options(name) VALUES 
                      """
        title_query = """ DROP TABLE IF EXISTS title; 
                        CREATE TABLE `title` (
                            `id` int AUTO_INCREMENT,
                           `title` varchar(254) NOT NULL,
                           `multiselect` int(5) NOT NULL,
                                PRIMARY KEY (`id`)
                          );
                          INSERT INTO title(title,multiselect)  VALUES """
         
        cursor = connection.cursor()
        print(title_query + titleOPTS + pollQuery + options)
        for result in cursor.execute(title_query + titleOPTS + pollQuery + options , multi=True):
            if result.with_rows:
                print("Rows produced by statement '{}':".format(
                result.statement))
                print(result.fetchall())
            else:
                print("Number of rows affected by statement '{}': {}".format(
                result.statement, result.rowcount))
        print ("Poll Oprions Table Created")
        connection.commit()
        cursor.close()
        #onnection.close()

        print ("Query Ran Sucessfully")
    except mysql.connector.Error as error :
        connection.rollback() #rollback if any exception occured
        print("Failed inserting record into python_users table {}".format(error))


    data = {'name': 'Alice', 'type': "createPoll", 'content': content}
    return jsonify(data)



@app.route('/check',methods = ['POST'])
def check1():
    print (request.is_json)
    uid =  '"' + request.get_json()['id'] + '"'
    checkQuery = "select ifNull(count( id ),0) from polls where polls.uid like " + uid 
    resultQuery = "select options.name as Options, count(polls.id) as Votes from polls  join options on options.name = polls.options group by polls.options "  
    giveOptions = "select options.name from options "  

    cursor = connection .cursor()
    cursor.execute(checkQuery)
    records = cursor.fetchall()
    print(records[0][0])

    cursorMain = connection .cursor()
    cursorMain.execute("select title.title, title.multiselect from title ")
    recordsMain = cursorMain.fetchall() 


    if(records[0][0] > 0 ):
        cursor2 = connection.cursor()
        cursor2.execute(resultQuery)
        results =  cursor2.fetchall()
        data = {"topic": recordsMain[0][0], "results":results , "flag": True}
        return jsonify(data)
    else:
        cursor2 = connection.cursor()
        cursor2.execute(giveOptions)
        options_tovote =  cursor2.fetchall()
        data = {"topic": recordsMain[0][0], "options":options_tovote , "multiselct" : recordsMain[0][1], "flag":False }
        return jsonify(data)

@app.route('/admin')
def about():
    return render_template('layouts/HTML/AdminPage.html')

@app.route('/results')
def results():
    return render_template('layouts/HTML/VotingResults.html')



@app.route('/vote_results',methods = ['POST'])
def vote_results():
    print (request.is_json)
    resultQuery = "select options.name as Options, count(polls.id) as Votes from polls join options on options.name = polls.options group by polls.options "  

    cursorMain = connection .cursor()
    cursorMain.execute("select title.title, title.multiselect from title ")
    recordsMain = cursorMain.fetchall() 

    cursor2 = connection.cursor()
    cursor2.execute(resultQuery)
    results =  cursor2.fetchall()
    print(results)
    data = {"topic": recordsMain[0][0], "results":results }
    return jsonify(data)




# Error handlers.
@app.errorhandler(500)
def internal_error(error):
    #db_session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404

# Launch.

# Default port:
if __name__ == '__main__':
    app.run()

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
'''
