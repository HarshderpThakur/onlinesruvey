SET UP ENVIRONMENT 

1. To install vertualenv global [one time]

-> python3 -m pip install --user virtualenv

2. Create Project Folder [Name Any]
3. Go to inside directory [ cd ]
4. Create your virtual environment [Any Name]
  --> python3 -m virtualenv env
5. To activate vertualenv
  --> source ./env/bin/activate
6. Use pip3 if you using python3 or pip for python2.7
7. To copy all library to requirement.txt
  --> pip3 list > requirement.txt

NOTE: For your reference the user name , password and database of MySql local host are already set in app.py file. You can change them accordingly.

